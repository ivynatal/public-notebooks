# Ivy Natal's public notebooks

This repository is for Jupyter notebooks that we want to make public outside the context of another specific project. The notebooks are intended to be viewed inside this repository (rendered by GitLab) or by using [Binder](https://mybinder.org/).

View this repository on Binder: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/ivynatal%2Fpublic-notebooks/master)

## Notes

The `test/` directory is just for a sandbox for us to test features of our local and deploy environments. It may include notebooks from other sources or ones we create for testing. Here are the credits for the notebooks from other sources:

- The notebooks under `test/norvig.org` are from [Peter Norvig](http://www.norvig.com/ipython/README.html).
